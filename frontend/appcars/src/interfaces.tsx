export interface DataService {
    loading: boolean;
    data: any;
    error?: any;
}

export interface Maintenance {
    deliverDate: string;
    name: string;
    lastName: string;
}

export interface Car {
    description: string;
    estimatedate: Maintenance;
    id: number;
    image: string;
    make: string;
    model: string;
    expanded: boolean;
    km: string;
}

export interface DataForm {
    name: string;
    lastName: string;
    idCar: number;
    deliverDate: any;
}