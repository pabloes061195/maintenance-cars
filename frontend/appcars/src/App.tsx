import React from 'react';
import Header from './components/header/header';
import './App.css';
import Main from './components/main/main';

export default function App() {
  return (
    <div>
      <Header/>
      <section>
        <Main/>
      </section>
    </div>
  );
}
