import 'date-fns';
import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useStyles } from './maintenance.mat-ui';
import { IconButton, TextField, CircularProgress } from '@material-ui/core';
import BuildIcon from '@material-ui/icons/Build';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { DataForm } from '../../interfaces';
import { addMaintenance } from '../../services/cars.services';
import CheckIcon from '@material-ui/icons/Check';
import { useDispatch } from 'react-redux';
import { addMaintenanceAction } from '../../redux/actions';

export default function Maintenance(props: any) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const [openLoading, setOpenLoading] = useState(false);
    const [dataForm, setDataForm] = useState<DataForm>({
        name: '',
        lastName: '',
        idCar: props.car.id,
        deliverDate: new Date()
    });

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDataForm({...dataForm, [event.target.id]: event.target.value});
      };
    
    const handleDateChange = (date: Date | null) => {
        setDataForm({...dataForm, deliverDate: date});
    };

    const submit = () => {
        setOpenLoading(true);
        handleClose();

        const request = {
            ...dataForm,
            deliverDate: dataForm.deliverDate !== null ? dataForm.deliverDate.getTime() : null
        }

        addMaintenance(request)
        .then(() => dispatch({type: addMaintenanceAction.type, car: request}))
        .finally(() => setOpenLoading(false));
    }

    return (
        <div className={classes.root}>
            <IconButton
                className={openLoading || props.car.estimatedate.deliverDate !== '' ? classes.displayNone : classes.expand}
                onClick={handleClickOpen}
                aria-label="add Maintenance">
                <BuildIcon color="secondary" />
            </IconButton>

            <div className={openLoading ? classes.expand : classes.displayNone}>
                <CircularProgress  color="secondary" />
            </div>

            <div className={props.car.estimatedate.deliverDate !== '' ? classes.expand : classes.displayNone}>
                <CheckIcon fontSize="large" color="secondary"/>
            </div>
           
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title">

                <DialogTitle id="responsive-dialog-title">{`MAINTENANCE CAR ${props.car.id}`}</DialogTitle>
                <DialogContent>
                <form className={classes.form} noValidate autoComplete="off">
                    <TextField value={dataForm.name} onChange={handleChange} className={classes.textForm} id="name" label="Name" />
                    <TextField value={dataForm.lastName} onChange={handleChange} className={classes.textForm} id="lastName" label="Last name" />
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        className={classes.textForm}
                        margin="normal"
                        id="date-picker-dialog"
                        label="Date picker dialog"
                        format="MM/dd/yyyy"
                        value={dataForm.deliverDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}/>
                    </MuiPickersUtilsProvider>
                </form>
                </DialogContent>

                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={submit} color="primary" autoFocus>
                        Crear
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
