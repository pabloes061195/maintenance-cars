import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            display: 'flex'
        },
        displayNone: {
            display: 'none'
        },
        form: {
            minWidth: '400px',
            display: 'flex',
            flexDirection: 'column',
            [theme.breakpoints.up('xs')]: {
                minWidth: '100%'
            },
            [theme.breakpoints.up('sm')]: {
                minWidth: '400px'
            }
        },
        textForm: {
            width: '100%',
            marginBottom: '36px'
        },
        expand: {
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
            }),
        }
    })
);