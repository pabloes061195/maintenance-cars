import React from 'react';
import { useStyles } from './header.mat-ui';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

export default function Header() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar variant="dense">
          <Typography variant="h6" color="inherit">
            Maintenance Cars
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
