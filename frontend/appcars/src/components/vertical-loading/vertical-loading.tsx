import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useStyles } from './vertical-loading.mat-ui';

export default function VerticalLoading() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <LinearProgress color="secondary" />
    </div>
  );
}