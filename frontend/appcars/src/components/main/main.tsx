import React, { useState, useEffect } from 'react';
import { getAllCars } from '../../services/cars.services';
import Cars from '../cars/cars';
import { DataService, Car } from '../../interfaces';
import './main.css';
import VerticalLoading from '../vertical-loading/vertical-loading';

export default function Main() {
    const [data, setData] = useState<DataService>({ loading: true, data: [] });

    useEffect(() => {
        console.log(data);
        if (data.loading) {
            getAllCars()
            .then((response: any) => {
                console.log(response);
                setData({
                    loading: false,
                    data: response.data.map((car: Car) => Object.assign(car, {expanded: false}))
                });
            })
            .catch((error) => setData({
                ...data,
                error: error,
                loading: false
            }));
        } 
    }, [data]);

    return (
        <div className="divParent">
            {
                data.loading ? <VerticalLoading/> : <Cars data={data.data} />
            }
        </div>
    )
}