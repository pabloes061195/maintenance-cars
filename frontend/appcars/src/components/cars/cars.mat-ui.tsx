import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { pink } from '@material-ui/core/colors';

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        rootParent: {
            padding: '0px',
            width: '100%',
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-around'
        },
        rootWhite: {
            backgroundColor: 'white'
        },
        displayNone: {
            display: 'none'
        },
        display: {
            display: 'block'
        },
        rootAgend: {
            backgroundColor: pink[100]
        },
        root: {
            width: 345,
            margin: '16px',
            [theme.breakpoints.up('xs')]: {
                width: '100%',
                margin: '16px 0px'
            },
            [theme.breakpoints.up('sm')]: {
                width: 'calc(50% - 32px)'
            },
            [theme.breakpoints.up('md')]: {
                width: 'calc(50% - 32px)'
            },
            [theme.breakpoints.up('lg')]: {
                width: 'calc(25% - 32px)'
            }

        },
        media: {
            height: 0,
            paddingTop: '56.25%'
        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
            }),
        },
        avatar: {
            backgroundColor: pink[500],
        },
    }),
);