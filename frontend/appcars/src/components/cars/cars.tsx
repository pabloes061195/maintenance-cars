import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './cars.mat-ui';
import { Car } from '../../interfaces';
import Maintenance from '../maintenance/maintenance';
import { useSelector } from 'react-redux';

export default function Cars(props: any) {
    const classes = useStyles();
    const maintenance: any = useSelector(reducers => reducers);
    const [cars, setCars] = useState<Car[]>([]);

    useEffect(() => {
        const dataCars = props.data.map((car: Car) => car.estimatedate !== undefined ? 
            {
                ...car, 
                estimatedate: {
                    ...car.estimatedate,
                    deliverDate: new Date(car.estimatedate.deliverDate).toLocaleDateString()
                }
            } : {
                ...car, 
                estimatedate: {
                    name: '',
                    lastName: '',
                    deliverDate: ''
                }
            } );

        setCars(dataCars);
    }, [props.data]);

    useEffect(() => {
        if (maintenance !== null) {
            const dataCars = cars.map((car: Car) => car.id === maintenance.idCar ? 
            {
                ...car, 
                estimatedate: {
                    ...maintenance.estimatedate,
                    deliverDate: new Date(maintenance.deliverDate).toLocaleDateString()
                }
            } : car );

            setCars(dataCars);
        }
    }, [maintenance]);

    return (
        <div className={classes.rootParent}>
            {
                cars.map((car: Car, index: number) =>
                    <Card key={car.id} className={classes.root}>
                        <CardHeader
                            className={car.estimatedate.deliverDate !== '' ? classes.rootAgend : classes.rootWhite}
                            avatar={
                                <Avatar color="secondary" aria-label="recipe" className={classes.avatar}>
                                    {index}
                                </Avatar>
                            }
                            title={`${car.make} - ${car.model}`}
                            subheader={`${car.km} km`} />

                        <CardMedia
                            className={classes.media}
                            image={car.image}
                            title={car.model} />

                        <CardContent>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {car.description}
                            </Typography>
                            <Typography 
                                className={car.estimatedate.deliverDate !== '' ? classes.display : classes.displayNone} 
                                variant="body2" color="textSecondary" component="p">
                                <b>{`Date: ${car.estimatedate.deliverDate}`}</b>
                            </Typography>
                        </CardContent>

                        <CardActions disableSpacing>
                            <Maintenance car={car}/>
                        </CardActions>
                    </Card>
                )
            }
        </div>
    );
}
