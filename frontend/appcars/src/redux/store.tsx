import { CHANGUE_CAR } from "./actions";
import { createStore } from 'redux';

const reducer = (state = null, actions: any) => {
    switch (actions.type) {
        case CHANGUE_CAR:
            return actions.car;
        default:
            return state;
    }
}

export default createStore(reducer);