import axios from 'axios';
import { BASE_URI } from '../props'
import { DataForm } from '../interfaces';

export const getAllCars = (): Promise<any> => {
    return axios.get(BASE_URI);
}

export const addMaintenance = (request: DataForm): Promise<any> => {
    return axios.post(`${BASE_URI}/newMaintenance`, request, { headers: {'Content-Type': 'application/json'}});
}