export interface PersonMaintenance {
    name: string;
    lastName: string;
    idCar: number;
    deliverDate: number;
}

export interface Car {
    description: string;
    estimatedate: string;
    id: number;
    image: string;
    make: string;
    model: string;
    km: string;
}