import { Request, Response } from 'express';
import firebase from '../db/initializer';
import { Car } from '../interfaces';

const db = firebase.database();

export const getCars = async (req: Request, res: Response) => {
    return await db.ref('cars').once('value')
        .then((data: any) => {
            const cars: any = data.val();

            if (cars === null) {
                res.status(204).send();
            }

            res.status(200).send(cars);
        })
        .catch(err => controlErrors(res, err, 'Error to try get information.'));
}

export const addMaintenance = async (req: Request, res: Response) => {
    return await db.ref('cars').once('value')
        .then((data: any) => {
            const cars: Car[] = data.val();
            const newCars = cars.map(car => car.id === req.body.idCar ? {...car, estimatedate: {
                deliverDate: req.body.deliverDate,
                name: req.body.name,
                lastName: req.body.lastName
            }} : car);

            return db.ref('cars').set(newCars);
        })
        .then(() => res.status(201).send())
        .catch(err => controlErrors(res, err, 'Error to try create new maintenance.'));
}

const controlErrors = (res: Response, err: any, message: string) => {
    if (err.message) {
        res.status(500).send(err.message);
    }

    res.status(500).send(message);
}