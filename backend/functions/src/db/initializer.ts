import admin from 'firebase-admin';
import serviceAccountKey from './serviceAccountKey.json';

const serviceAccount: any = serviceAccountKey;

export default admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://auth-firebase-265717.firebaseio.com"
});