import * as functions from 'firebase-functions';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { getCars, addMaintenance } from './resolvers/cars';

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/', getCars);
app.post('/newMaintenance', addMaintenance);

//app.listen(4001, () => console.log('sever listen'));

export const cars = functions.https.onRequest(app);
